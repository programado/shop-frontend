import PrimeVue from 'primevue/config'

// Components
import Button from 'primevue/button'
import Terminal from 'primevue/terminal'

// Directives
import Ripple from 'primevue/ripple'
import StyleClass from 'primevue/styleclass'
import BadgeDirective from 'primevue/badgedirective'
import Tooltip from 'primevue/tooltip'

// Services
import ToastService from 'primevue/toastservice'
import ConfirmationService from 'primevue/confirmationservice'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(PrimeVue, {
    ripple: true,
    locale: {
      accept: 'OK',
      reject: 'Abbrechen',
    },
  })

  nuxtApp.vueApp.use(PrimeVue, { ripple: true })

  nuxtApp.vueApp.use(ToastService)
  nuxtApp.vueApp.use(ConfirmationService)

  nuxtApp.vueApp.component('Button', Button)
  nuxtApp.vueApp.component('Terminal', Terminal)

  nuxtApp.vueApp.directive('ripple', Ripple)
  nuxtApp.vueApp.directive('styleclass', StyleClass)
  nuxtApp.vueApp.directive('badge', BadgeDirective)
  nuxtApp.vueApp.directive('tooltip', Tooltip)
})
